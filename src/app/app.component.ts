import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  @ViewChild('form') form: NgForm;

  defaultSubscription: string = 'advanced';
  subscriptions: Array<string> = [
    'Basic',
    'Advanced',
    'Pro'
  ];

  onSubmit() {
    console.log(this.form.value);
  }
}
